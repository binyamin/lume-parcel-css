# ParcelCSS for Lume

A _temporary_ [ParcelCSS](https://github.com/parcel-bundler/parcel-css) plugin
for [Lume](https://lume.land), a static-site generator for
[Deno](https://deno.land).

## Usage

Import the module from GitLab, using https://denopkg.dev:

```js
import parcel_css from 'https://denopkg.dev/gl/binyamin/lume-parcel-css@v0.1.2/mod.ts';
```

## Legal

All source-code is provided under the terms of
[the MIT license](https://gitlab.com/binyamin/lume-parcel-css/-/blob/main/LICENSE).
Copyright 2022 Binyamin Aron Green.
