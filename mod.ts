//@deno-types="https://esm.sh/@parcel/css-wasm@1.11.2"
import init, {
	transform,
	type TransformOptions,
} from 'https://unpkg.com/@parcel/css-wasm@1.11.2/index.js';
// } from 'https://deno.land/x/lume@v1.10.0/deps/parcel_css.ts';
import { merge } from 'https://deno.land/x/lume@v1.10.0/core/utils.ts';
import { Page } from 'https://deno.land/x/lume@v1.10.0/core/filesystem.ts';
import {
	basename,
	dirname,
	relative,
} from 'https://deno.land/x/lume@v1.10.0/deps/path.ts';

import type { Site } from 'https://deno.land/x/lume@v1.10.0/core.ts';

export interface Options {
	/** The list of extensions this plugin applies to */
	extensions: string[];

	sourceMap: boolean;

	/** Options passed to parcel_css */
	options: Omit<TransformOptions, 'inputSourceMap' | 'filename' | 'code'>;
}

// Default options
export const defaults: Options = {
	extensions: ['.css'],
	sourceMap: false,
	options: {
		minify: true,
		drafts: {
			nesting: true,
			customMedia: true,
		},
		targets: {
			android: version(98),
			chrome: version(98),
			edge: version(98),
			firefox: version(97),
			ios_saf: version(15),
			safari: version(15),
			opera: version(83),
			samsung: version(16),
		},
	},
};

// Init parcelCSS
await init();

/** A plugin to load all CSS files and process them using parcelCSS */
export default function (userOptions?: Partial<Options>) {
	return (site: Site) => {
		const options = merge(defaults, userOptions);

		site.loadAssets(options.extensions);
		site.process(options.extensions, parcelCSS);

		function parcelCSS(file: Page) {
			const to = site.dest(file.dest.path + file.src.ext);
			const from = site.src(file.src.path + file.src.ext);

			if (options.sourceMap) {
				options.options.sourceMap = options.sourceMap;
			}

			const mapFile = site.pages.find((page) =>
				page.data.url === file.data.url + '.map'
			);

			// Process the code with parcelCSS
			const content = typeof file.content === 'string'
				? new TextEncoder().encode(file.content)
				: file.content as Uint8Array;

			const transformOptions: TransformOptions = {
				filename: relative(dirname(to), from),
				code: content,
				inputSourceMap: mapFile?.content as string | undefined,
				...options.options,
			};

			const result = transform(transformOptions);
			const decoder = new TextDecoder();

			file.content = decoder.decode(result.code);

			if (result.map) {
				if (mapFile) {
					mapFile.content = decoder.decode(result.map);
				} else {
					const mapFile = Page.create(
						file.dest.path + '.css.map',
						decoder.decode(result.map),
					);
					site.pages.push(mapFile);
				}

				file.content += `\n/*# sourceMappingURL=./${
					basename(file.dest.path)
				}.css.map */`;
			}
		}
	};
}

/**
 * Convert a version number to a single 24-bit number
 */
export function version(major: number, minor = 0, patch = 0): number {
	return (major << 16) | (minor << 8) | patch;
}
