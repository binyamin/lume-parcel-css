import type * as bindings from './wasm-bindings.ts';
import { default as sourcemap } from 'https://esm.sh/@parcel/source-map@2.0.5/index.d.ts';

export default class extends sourcemap {
	sourceMapInstance: bindings.SourceMap;
	projectRoot: string;
}
