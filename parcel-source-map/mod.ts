// @deno-types="./SourceMap.d.ts"
import SourceMap from 'https://esm.sh/@parcel/source-map@2.0.5/dist/SourceMap.js';

import type {
	GenerateEmptyMapOptions,
	VLQMap,
} from 'https://esm.sh/@parcel/source-map@2.0.5/index.d.ts';

import * as bindings from './wasm-bindings.ts';

import { Buffer } from 'https://deno.land/std@0.147.0/node/buffer.ts';

const url =
	'https://esm.sh/@parcel/source-map@2.0.5/parcel_sourcemap_wasm/dist-web/parcel_sourcemap_wasm_bg.wasm';

export const init = () => bindings.init(url);

export type {
	GenerateEmptyMapOptions,
	IndexedMapping,
	MappingPosition,
	ParsedMap,
	SourceMapStringifyOptions,
	VLQMap,
} from 'https://esm.sh/@parcel/source-map@2.0.5/index.d.ts';

export default class WasmSourceMap extends SourceMap {
	constructor(projectRoot: string = '/', buffer?: Buffer) {
		super(projectRoot, buffer);
		this.sourceMapInstance = new bindings.SourceMap(projectRoot, buffer);
		this.projectRoot = this.sourceMapInstance.getProjectRoot();
	}

	addVLQMap(
		map: VLQMap,
		lineOffset: number = 0,
		columnOffset: number = 0,
	): SourceMap {
		let { sourcesContent, sources = [], mappings, names = [] } = map;
		if (!sourcesContent) {
			sourcesContent = sources.map(() => '');
		} else {
			sourcesContent = sourcesContent.map((
				content,
			) => (content ? content : ''));
		}
		this.sourceMapInstance.addVLQMap(
			mappings,
			sources,
			sourcesContent.map((content) => (content ? content : '')),
			names,
			lineOffset,
			columnOffset,
		);
		return this;
	}

	addSourceMap(sourcemap: SourceMap, lineOffset: number = 0): SourceMap {
		if (!(sourcemap.sourceMapInstance instanceof bindings.SourceMap)) {
			throw new Error(
				'The sourcemap provided to addSourceMap is not a valid sourcemap instance',
			);
		}

		this.sourceMapInstance.addSourceMap(
			sourcemap.sourceMapInstance,
			lineOffset,
		);
		return this;
	}

	addBuffer(buffer: Buffer, lineOffset: number = 0): SourceMap {
		let previousMap = new WasmSourceMap(this.projectRoot, buffer);
		return this.addSourceMap(previousMap, lineOffset);
	}

	extends(input: Buffer | SourceMap): SourceMap {
		// $FlowFixMe
		let inputSourceMap: SourceMap = input instanceof Uint8Array
			? new WasmSourceMap(this.projectRoot, input)
			: input;
		this.sourceMapInstance.extends(inputSourceMap.sourceMapInstance);
		return this;
	}

	delete() {
		this.sourceMapInstance.free();
	}

	static generateEmptyMap({
		projectRoot,
		sourceName,
		sourceContent,
		lineOffset = 0,
	}: GenerateEmptyMapOptions): WasmSourceMap {
		let map = new WasmSourceMap(projectRoot);
		map.addEmptyMap(sourceName, sourceContent, lineOffset);
		return map;
	}
}
