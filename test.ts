import SourceMap, { init } from './parcel-source-map/mod.ts';

Deno.test('@parcel/source-map', async () => {
	await init();

	const map = new SourceMap('css');
	map.toVLQ();
});
